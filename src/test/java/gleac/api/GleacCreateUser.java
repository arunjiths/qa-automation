package gleac.api;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import gleac.pages.SignupPage;
import gleac.testcases.MinibenchmarkPageTest;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class GleacCreateUser extends MinibenchmarkPageTest {
	
	
	
@Test
	public void CreateProfile(String firstname,String lastname, String id,String password)  {
		
		//specify base URI
		RestAssured.baseURI="http://usersapi.gleactest.com/api";	
		//Request object- to send request
		RequestSpecification httprequest=RestAssured.given();



		//JSON Request Payload
		JSONObject requestParams =new JSONObject();

		requestParams.put("firstName",firstname );
		requestParams.put("lastName", lastname	);
		requestParams.put("email", id);
		requestParams.put("password", 	password);

		httprequest.header("Content-Type","application/json");
		httprequest.body(requestParams.toJSONString());
		//response object
		Response response =httprequest.request(Method.POST, "/Users" );
		
		// Printing reposnse

		String responsebody=response.getBody().asString();
		System.out.println("Response body for new user : " +responsebody);
		
				//Status code Validation
		
		int statusCode=response.getStatusCode();
		System.out.println("Status code for new user" +responsebody);
		Assert.assertEquals(statusCode, 200);
		
		String code=response.jsonPath().get("result");
		
		
		
		System.out.println(code);

		
		
		RestAssured.baseURI="http://usersapi.gleactest.com/api/Users/account";	
		//Request object- to send request
		RequestSpecification httprequest2=RestAssured.given();



		//JSON Request Payload
		JSONObject requestParams2 =new JSONObject();

		
		requestParams.put("email", id);
		requestParams.put("verificationCode", code);

		httprequest2.header("Content-Type","application/json");
		httprequest2.body(requestParams.toJSONString());
		//response object
		Response response2 =httprequest2.request(Method.POST, "/verify" );
		
		// Printing reposnse

		String responsebody2=response2.getBody().asString();
		
		
				//Status code Validation
		
//		int statusCode2=response2.getStatusCode();
//		System.out.println("Status code is" +responsebody2);
//		Assert.assertEquals(statusCode2, 200);
		
	System.out.println(responsebody2);
		
		
		


		

	}
}