package gleac.testcases;

import org.testng.annotations.Test;

import com.aventstack.extentreports.model.Log;

import java.awt.AWTException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import gleac.api.GleacCreateUser;
import gleac.api.GleacGETRequest;


//import org.testng.Assert;
//import org.testng.annotations.BeforeMethod;
//import org.testng.annotations.DataProvider;
//import org.testng.annotations.Test;
//
//import com.relevantcodes.extentreports.model.Log;

import gleac.base.TestBase;
import gleac.pages.MinibenchmarkPage;
import gleac.pages.SignupPage;
import gleac.util.TestUtil;
import junit.framework.Assert;
import gleac.util.Lib;

public class MinibenchmarkPageTest extends TestBase{

	MinibenchmarkPage minibenchmark;
	SignupPage signuppage;
	GleacGETRequest gr;
	GleacCreateUser createuser;
	
	String firstname;
	String lastname;
	String password;
	public String id;

	public void signup() throws InterruptedException, AWTException {

//		signuppage=new SignupPage();
		//gr=new GleacGETRequest();
		String firstname=	Lib.getcelldata("LoginData", "Login", 2, 0);
		String lastname=	Lib.getcelldata("LoginData", "Login", 2, 1);
		String email=	Lib.getcelldata("LoginData", "Login", 2, 2);
		id=email+Math.random()+TestUtil.domain;
		password=Lib.getcelldata("LoginData", "Login", 2, 3);
		String confirmpwd=	Lib.getcelldata("LoginData", "Login", 2, 4);
		//signuppage.CreateUser(firstname, lastname, id, password, confirmpwd);	
		
		GleacCreateUser createUser=new GleacCreateUser();
		createUser.CreateProfile(firstname, lastname, id, password);
		minibenchmark.UserLogin(id,password);		
//		signuppage.clicksignupbtn();			
//		Thread.sleep(3000);
		//gr.getVerificationCode(id);

//		Integer num=Integer.valueOf(gr.code);
//		signuppage.sendOTP(num);
	}


	
	@Test(priority=1)
	public void Minitest() throws InterruptedException, AWTException {

		//Click on the takebenchmarktest
		minibenchmark=new MinibenchmarkPage();
		
		minibenchmark.ClickonBenchmark();
		minibenchmark.ClickonRole();
		minibenchmark.ClickOptionAnswer();
		signup();
		minibenchmark.NavigateResult();
		//Resend
		//getCode();
	}
	
	
	@Test(dependsOnMethods={"Minitest"})
	public void Minitestretake() throws InterruptedException, AWTException {

		//Click on the takebenchmarktest
		minibenchmark=new MinibenchmarkPage();
		minibenchmark.ClickonBenchmark();
		minibenchmark.ClickonRole();
		minibenchmark.ClickOptionAnswer();
		minibenchmark.UserLogin(id,password);	
		Thread.sleep(5000);
		minibenchmark.TakeBenchmarkAgain();
		//Resend
		//getCode();

	}
	
	
	@Test(priority=2)
	public void Minitesterrorvalidation() throws InterruptedException, AWTException {

		//Click on the takebenchmarktest
		minibenchmark=new MinibenchmarkPage();
		minibenchmark.ClickonBenchmark();
		minibenchmark.ClickonRole();
		minibenchmark.ClickOptionAnswer();
		minibenchmark.UserLogin(id,password+1);	
		minibenchmark.errormessage();
		
		//Resend
		//getCode();

	}
	

}



