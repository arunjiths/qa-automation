package gleac.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import gleac.base.TestBase;
import junit.framework.Assert;


public class MinibenchmarkPage extends TestBase{

	//OR
	@FindBy(xpath = "//div[@class='left-panel']//button[@class='btn btn-mini-benchmark'][normalize-space()='TAKE THE BENCHMARK']")
	WebElement TakeBenchmark;
	@FindBy(xpath="//button[normalize-space()='Data Scientist']")
	WebElement Role;
	@FindBy(xpath="//button[normalize-space()='Agree']")
	WebElement OptionAnswer;
	@FindBy(xpath="//button[normalize-space()='RESULTS, THANK YOU ;-)']")
	WebElement miniresult;
	@FindBy(xpath="//span[@class='gleac-link']")
	WebElement msignup;

	@FindBy(xpath ="//input[@placeholder='Registered Email']")
	WebElement registeredEmail;

	@FindBy(xpath ="//input[@placeholder='Enter Password']")
	WebElement enterPassword;

	@FindBy(xpath ="//button/span[contains(text(),'Show my Report ')]")
	WebElement showReport;

	@FindBy(xpath="//button[@class='btn btn-mini-benchmark-navigation']")
	WebElement navigateBtn;

	@FindBy(xpath="//*[@class='current-question-area']")
	WebElement TakeBenchmarkBtn;	

	@FindBy(xpath="//*[text()= 'Incorrect username/password.']")
	WebElement errormsg;



	//Initializing the page object
	public MinibenchmarkPage() {

		PageFactory.initElements(driver, this);

	}
	//Actions
	public String Benchmarkdisplayed() {

		return driver.getTitle();


	}


	public void ClickonBenchmark() {


		//		TakeBenchmark.getText();
		TakeBenchmark.click();

	}
	public void ClickonRole() {
		//		Role.isDisplayed();
		Role.click();

	}

	public void ClickOptionAnswer() throws InterruptedException {
		OptionAnswer.isDisplayed();
		int i=0;
		while(i<=9	) {
			if(i%2==0) {
				OptionAnswer.click();
				Thread.sleep(300);
				i++;
			}
			else {
				OptionAnswer.click();
				i++;
			}

		}

		miniresult.click();	
		Thread.sleep(1000);
		// MiniSignup();

	}


	public void UserLogin(String id,String password) {
		registeredEmail.sendKeys(id);
		enterPassword.sendKeys(password);
		showReport.click();

	}


	public void NavigateResult() {

		navigateBtn.click();
	}

	public String value;
	public void TakeBenchmarkAgain() {
    value=TakeBenchmarkBtn.getText();

	}


	public void errormessage() throws InterruptedException {
		Thread.sleep(3000);
		String expected="Incorrect username/password.";
		String errorms=	errormsg.getText();	
		Assert.assertEquals("The message is", expected,errorms);
	}

	//	
	//	public void MiniSignup() {
	//		
	//		msignup.click();
	//		
	//	}


}