package gleac.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.DataProvider;


import gleac.base.TestBase;

import gleac.pages.SignupPage;

public class Lib implements TestUtil{

	static Workbook book;
	static WebDriver driver;
	


		public static String getcelldata(String ExcelFilename,String sheetname,int row,int column) {
	
	
			String cellvalue=" ";
			try {
				FileInputStream fis=new FileInputStream(EXCEL_FILE_PATH +ExcelFilename + ".xlsx");
				Workbook wb=WorkbookFactory.create(fis);
				Sheet sheet	= wb.getSheet(sheetname);
				Row ro = sheet.getRow(row);
				Cell cell= ro.getCell(column);
				cellvalue=cell.toString();
			}
			catch(Exception e){
				e.printStackTrace();
			}
			return cellvalue;
	
	
		}

		
		public static void Scrolldown(WebElement object) {
			
			//WebDriverWait wait = new WebDriverWait(driver, 10);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", object);
			
			}
		
		public static void JavaScriptClick(WebElement object2) {
//			WebDriverWait wait=new WebDriverWait(driver, 20);
//			wait.until(ExpectedConditions.elementToBeClickable(object2));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		
		executor.executeScript("arguments[0].click();", object2);
		}
		
		
		public static ExpectedCondition<WebElement> elementToBeClickable(final WebElement element) {
			return new ExpectedCondition() {
				public WebElement apply(WebDriver driver) {
					WebElement visibleElement = (WebElement) ExpectedConditions.visibilityOf(element).apply(driver);

					try {
						return visibleElement != null && visibleElement.isEnabled() ? visibleElement : null;
					} catch (StaleElementReferenceException arg3) {
						return null;
					}
				}

				public String toString() {
					return "element to be clickable: " + element;
				}

				@Override
				public Object apply(Object input) {
					// TODO Auto-generated method stub
					return null;
				}
			};
		}


		
		
		
		
		
//	@DataProvider(name = "LoginData")
//	public static Object[][]getTestData(String sheetName){
//		FileInputStream file = null;
//		try {
//			file = new FileInputStream(EXCEL_FILE_PATH);
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		}
//		try {
//			book=WorkbookFactory.create(file);
//		}catch (InvalidFormatException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		Sheet sheet=book.getSheet(sheetName);
//		Object[][] data = new Object[sheet.getLastRowNum()][sheet.getRow(0).getLastCellNum()];
//		// System.out.println(sheet.getLastRowNum() + "--------" +
//		// sheet.getRow(0).getLastCellNum());
//		for (int i = 0; i < sheet.getLastRowNum(); i++) {
//			for (int k = 0; k < sheet.getRow(0).getLastCellNum(); k++) {
//				data[i][k] = sheet.getRow(i + 1).getCell(k).toString();
//				// System.out.println(data[i][k]);
//			}
//		}
//
//
//
//		return data;
//
//
//	}
		
		
//		
//		public static void Javascript(WebElement tobeclicked) {	
//			
//	
//		}
	
//		public static void Scrollverticallydown() throws InterruptedException {
//		jse = (JavascriptExecutor) driver;  
//		for (int i = 1; i < 10; i++) {
//			//scroll down on the webpage
//			jse.executeScript("window.scrollBy(0, 100)");
//			Thread.sleep(3000);
//		}
		
	
		
		
//		
//		}
		
		public static void MovetoElement(WebElement link) {
	// TODO Auto-generated method stub
			Actions actions = new Actions(driver);
			actions.doubleClick(link).perform();
	
}




		public static void waitforelement(String element) {
		WebDriverWait w = new WebDriverWait(driver,100);
		w.until(ExpectedConditions.presenceOfElementLocated (By.xpath(element)));
		}
		
		
		
		
	
		public static String getScreenshot(WebDriver driver)
		{
			TakesScreenshot ts=(TakesScreenshot) driver;
			
			File src=ts.getScreenshotAs(OutputType.FILE);
			
			String path=System.getProperty("user.dir")+"/Screenshot/"+System.currentTimeMillis()+".png";
			
			File destination=new File(path);
			
			try 
			{
				FileUtils.copyFile(src, destination);
			} catch (IOException e) 
			{
				System.out.println("Capture Failed "+e.getMessage());
			}
			
			return path;
		}


		
	
	
	}
